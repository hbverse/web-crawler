CREATE TABLE search_result (
  id            bigint(20)   NOT NULL AUTO_INCREMENT,
  title         varchar(255) NOT NULL UNIQUE,
  url           varchar(255) NOT NULL UNIQUE,
  date          TIMESTAMP    NOT NULL,
  opened        TINYINT      NOT NULL DEFAULT 0,
  search_key_id bigint(20)   NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (search_key_id) REFERENCES search_key (id)
    ON UPDATE CASCADE
    ON DELETE CASCADE
)
