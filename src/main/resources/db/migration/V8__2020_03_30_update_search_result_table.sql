ALTER TABLE search_result
    CHANGE `date` `search_date` TIMESTAMP NOT NULL;
ALTER TABLE search_result
    CHANGE `hide` `hidden` TINYINT(1) DEFAULT 0 NOT NULL;
ALTER TABLE search_result
    MODIFY `published_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE search_result
    MODIFY `fb_url` VARCHAR(255) UNIQUE NOT NULL DEFAULT 'default';
