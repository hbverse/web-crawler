CREATE TABLE willhaben_user (
  id       bigint(20)   NOT NULL AUTO_INCREMENT,
  email    varchar(255) NOT NULL UNIQUE,
  password varchar(255) NOT NULL UNIQUE,
  PRIMARY KEY (id)
)
