CREATE TABLE search_key (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  value varchar(255) NOT NULL UNIQUE,
  PRIMARY KEY (id)
)

