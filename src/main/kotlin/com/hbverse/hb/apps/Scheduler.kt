package com.hbverse.hb.apps

import com.hbverse.hb.apps.wiener_zeitung.mail.CrawlerMailSender
import com.hbverse.hb.apps.wiener_zeitung.service.SearchKeyService
import com.hbverse.hb.apps.wiener_zeitung.service.SearchPeriod
import com.hbverse.hb.apps.wiener_zeitung.service.SearchResultService
import org.slf4j.Logger
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component

@Component
class Scheduler(
    private val searchResultService: SearchResultService,
    private val searchKeyService: SearchKeyService,
    private val crawlerMailSender: CrawlerMailSender,
    private val log: Logger
) {

    @Scheduled(cron = "0 0 20 * * *")
    fun runCrawler() {
        val searchKeys = searchKeyService.getAllSearchKeys()
        val excludeKeys = searchKeyService.getAllExcludingKeys()
        val results = searchResultService.getNewResults(searchKeys, excludeKeys, SearchPeriod.CURRENT_WEEK)
        if (results.isNotEmpty()) {
            crawlerMailSender.sendSearchResults(results)
        } else {
            log.info("No new search results were found. Skipping mail sending.")
        }
    }

}
