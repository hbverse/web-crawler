package com.hbverse.hb.apps.configuration

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding
import org.springframework.context.annotation.Profile
import org.springframework.validation.annotation.Validated
import javax.validation.constraints.NotBlank

@Validated
@ConfigurationProperties(prefix = "app", ignoreUnknownFields = false, ignoreInvalidFields = false)
@ConstructorBinding
class AppProperties(
    @NotBlank
    val baseUrl: String
)

@Validated
@ConfigurationProperties(prefix = "mail", ignoreUnknownFields = false, ignoreInvalidFields = false)
@ConstructorBinding
class MailProperties(
    val recipients: List<String> = emptyList(),
    @NotBlank
    val sender: String,
    @NotBlank
    val subject: String,
    @NotBlank
    val personal: String
)

@Profile("!dev && !test")
@Validated
@ConfigurationProperties(prefix = "sentry", ignoreInvalidFields = false, ignoreUnknownFields = false)
@ConstructorBinding
class SentryProperties(
    @NotBlank
    val dsn: String?
)
