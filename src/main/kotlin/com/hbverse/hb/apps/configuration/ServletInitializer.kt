package com.hbverse.hb.apps.configuration

import com.hbverse.hb.apps.HbAppsApplication
import org.springframework.boot.builder.SpringApplicationBuilder
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer

class ServletInitializer : SpringBootServletInitializer() {

    override fun configure(application: SpringApplicationBuilder): SpringApplicationBuilder {
        return application.sources(HbAppsApplication::class.java)
    }

}

