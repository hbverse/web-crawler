package com.hbverse.hb.apps.configuration

import com.gargoylesoftware.htmlunit.BrowserVersion
import com.gargoylesoftware.htmlunit.IncorrectnessListener
import com.gargoylesoftware.htmlunit.SilentCssErrorHandler
import com.gargoylesoftware.htmlunit.WebClient
import com.gargoylesoftware.htmlunit.html.parser.HTMLParserListener
import com.gargoylesoftware.htmlunit.javascript.SilentJavaScriptErrorListener
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.net.URL
import java.util.logging.Level
import java.util.logging.Logger.getLogger

@Configuration
class WebClientConfiguration {

    init {
        getLogger("com.gargoylesoftware").level = Level.OFF
        getLogger("org.apache.commons").level = Level.OFF
    }

    @Bean
    fun webClient() = WebClient(BrowserVersion.CHROME).apply {
        options.apply {
            isCssEnabled = false
            isJavaScriptEnabled = true
            isThrowExceptionOnScriptError = false
            isThrowExceptionOnFailingStatusCode = false
        }
        cookieManager.isCookiesEnabled = true
        incorrectnessListener = IncorrectnessListener { _, _ -> }
        cssErrorHandler = SilentCssErrorHandler()
        javaScriptErrorListener = SilentJavaScriptErrorListener()
        htmlParserListener = object : HTMLParserListener {
            override fun warning(message: String?, url: URL?, html: String?, line: Int, column: Int, key: String?) {
            }

            override fun error(message: String?, url: URL?, html: String?, line: Int, column: Int, key: String?) {
            }
        }
    }

}
