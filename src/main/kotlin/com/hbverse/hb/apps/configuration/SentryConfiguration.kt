package com.hbverse.hb.apps.configuration

import io.sentry.Sentry
import io.sentry.spring.SentryExceptionResolver
import io.sentry.spring.SentryServletContextInitializer
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile

@Configuration
@Profile("!dev && !test")
class SentryConfiguration(
    properties: SentryProperties
) {

    init {
        Sentry.init(properties.dsn)
    }

    @Bean
    fun sentryExceptionResolver() = SentryExceptionResolver()

    @Bean
    fun sentryServletContextInitializer() = SentryServletContextInitializer()

}
