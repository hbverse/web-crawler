package com.hbverse.hb.apps.wiener_zeitung.entity

import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType.IDENTITY
import javax.persistence.Id
import javax.persistence.OneToOne

@Entity
class SearchResult(
    @Id
    @GeneratedValue(strategy = IDENTITY)
    val id: Long = -1,
    val title: String,
    val url: String,
    val searchDate: LocalDateTime,
    var opened: Boolean = false,
    var hidden: Boolean = false,
    var publishedAt: LocalDate,
    var fbUrl: String,
    @OneToOne
    val searchKey: SearchKey
) {

    fun getFormattedSearchDate(): String {
        return searchDate.format(DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm"))
    }

    fun getFormattedPublishedDate(): String {
        return publishedAt.format(DateTimeFormatter.ofPattern("dd.MM.yyyy"))
    }

    fun matches(key: SearchKey): Boolean {
        return key.value.toLowerCase() in title.toLowerCase()
    }

    fun matchesAny(keys: Collection<SearchKey>): Boolean {
        return keys.any { this.matches(it) }
    }

    fun hide() {
        hidden = true
    }

}
