package com.hbverse.hb.apps.wiener_zeitung.service

import com.hbverse.hb.apps.wiener_zeitung.entity.SearchKey
import com.hbverse.hb.apps.wiener_zeitung.entity.SearchResult
import com.hbverse.hb.apps.wiener_zeitung.service.SearchPeriod.CURRENT_WEEK

interface SearchResultService {

    fun getAllSearchResults(): List<SearchResult>

    fun getSearchResultById(id: Long): SearchResult

    fun getNewResults(searchKeys: List<SearchKey>, excludeKeys: List<SearchKey> = listOf(), period: SearchPeriod = CURRENT_WEEK): List<SearchResult>

    fun updateSearchResult(searchResult: SearchResult)

    fun hideMatchingResults(excludeKey: SearchKey)

}

