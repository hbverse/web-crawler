package com.hbverse.hb.apps.wiener_zeitung.controller

import com.hbverse.hb.apps.configuration.AppProperties
import com.hbverse.hb.apps.wiener_zeitung.service.SearchResultService
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping

@Controller
@RequestMapping("/crawler/mail")
class MailController(
    private val searchResultService: SearchResultService,
    private val configProps: AppProperties
) {

    @GetMapping
    fun getMailPage(model: Model): String {
        model.apply {
            addAttribute("searchResults", searchResultService.getAllSearchResults())
            addAttribute("baseUrl", configProps.baseUrl)
        }
        return "crawler/mail"
    }

}
