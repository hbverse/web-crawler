package com.hbverse.hb.apps.wiener_zeitung.repository

import com.hbverse.hb.apps.wiener_zeitung.entity.SearchResult
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface SearchResultRepository : JpaRepository<SearchResult, Long> {

    fun findAllByHiddenEqualsOrderByPublishedAtDesc(hidden: Boolean = false): List<SearchResult>

    fun findOneByTitleOrUrl(title: String, url: String): Optional<SearchResult>

}
