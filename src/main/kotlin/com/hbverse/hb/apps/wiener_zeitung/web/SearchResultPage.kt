package com.hbverse.hb.apps.wiener_zeitung.web

import com.gargoylesoftware.htmlunit.WebClient
import com.gargoylesoftware.htmlunit.html.*
import com.hbverse.hb.apps.wiener_zeitung.entity.SearchKey
import com.hbverse.hb.apps.wiener_zeitung.entity.SearchResult
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class SearchResultPage(
    private val page: HtmlPage,
    private val searchKey: SearchKey,
    private val baseUrl: String,
    private val webClient: WebClient
) {

    val results by lazy {
        val resultContainer = page.getElementById(HtmlElements.RESULT_CONTAINER_CHILD_ID).parentNode
        val resultListItems = resultContainer.childNodes[3].childNodes[1].childNodes.filterIsInstance<HtmlListItem>()
        return@lazy resultListItems
            .map { it.childNodes.first() as HtmlAnchor }
            .map { anchor ->
                val url = "$baseUrl/${anchor.hrefAttribute}"
                val details = getResultDetails(url)
                return@map SearchResult(
                    title = anchor.asText().replaceAfter("\n", "").removeSuffix("\n"),
                    url = url,
                    searchDate = LocalDateTime.now(),
                    searchKey = searchKey,
                    fbUrl = details.fbUrl,
                    publishedAt = details.publishedAt
                )
            }
    }

    private fun getResultDetails(resultUrl: String): SearchResultDetails {
        val companyPage = webClient.getPage<HtmlPage>(resultUrl)
        val fbAnchor = companyPage.getByXPath<DomElement>("//*[text()='FB Neueintragung']").first() as HtmlAnchor
        val date = fbAnchor.parentNode.parentNode.children.filterIsInstance<HtmlTableDataCell>().first().textContent.trim()
        return SearchResultDetails(
            fbUrl = "${baseUrl}/Secure/${fbAnchor.hrefAttribute}",
            publishedAt = LocalDate.parse(date, DateTimeFormatter.ofPattern("dd.MM.yyyy"))
        )
    }

    private data class SearchResultDetails(val fbUrl: String, val publishedAt: LocalDate)


    private object HtmlElements {
        const val RESULT_CONTAINER_CHILD_ID = "cnt"
    }

}
