package com.hbverse.hb.apps.wiener_zeitung.controller

import com.hbverse.hb.apps.wiener_zeitung.entity.SearchKey
import com.hbverse.hb.apps.wiener_zeitung.service.SearchKeyService
import com.hbverse.hb.apps.wiener_zeitung.service.SearchPeriod
import com.hbverse.hb.apps.wiener_zeitung.service.SearchResultService
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.*

@Controller
@RequestMapping("/crawler")
class CrawlerController(
    private val searchResultService: SearchResultService,
    private val searchKeyService: SearchKeyService
) {

    private var searchResultsCount: Int? = null

    @GetMapping
    fun getHomePage(model: Model): String {
        model.apply {
            addAttribute("searchResults", searchResultService.getAllSearchResults())
            addAttribute("searchKeys", searchKeyService.getAllSearchKeys())
            addAttribute("excludeKeys", searchKeyService.getAllExcludingKeys())
            addAttribute("searchResultsCount", searchResultsCount)
            addAttribute("searchPeriods", SearchPeriod.values().toList().sortedBy { it.index })
        }
        searchResultsCount = null
        return "crawler/home"
    }

    @PostMapping("/search-keys")
    fun addSearchKey(@RequestParam("value") value: String): String {
        searchKeyService.save(SearchKey(value = value))
        return redirectToCrawlerPage()
    }

    @PostMapping("/search-keys/exclude")
    fun addExcludeSearchKey(@RequestParam("value") value: String): String {
        val excludeKey = SearchKey(value = value, exclude = true)
        searchKeyService.save(excludeKey)
        searchResultService.hideMatchingResults(excludeKey)
        return redirectToCrawlerPage()
    }

    @DeleteMapping("/search-keys/{searchKeyId}")
    fun deleteSearchKey(@PathVariable("searchKeyId") searchKeyId: Long): String {
        searchKeyService.delete(searchKeyId)
        return redirectToCrawlerPage()
    }

    @PostMapping("/search")
    fun search(@RequestParam("period") periodIndex: Int): String {
        val searchKeys = searchKeyService.getAllSearchKeys()
        val excludeKeys = searchKeyService.getAllExcludingKeys()
        searchResultsCount = searchResultService.getNewResults(searchKeys, excludeKeys, SearchPeriod.ofIndex(periodIndex)).size
        return redirectToCrawlerPage()
    }

    @GetMapping("/search-results/{searchResultId}")
    fun redirectToSearchResult(@PathVariable("searchResultId") searchResultId: Long): String {
        val searchResult = searchResultService.getSearchResultById(searchResultId)
        searchResult.opened = true
        searchResultService.updateSearchResult(searchResult)
        return "redirect:${searchResult.url}"
    }

    @PutMapping("/search-results/{searchResultId}")
    fun hideSearchResult(@PathVariable("searchResultId") searchResultId: Long): String {
        val searchResult = searchResultService.getSearchResultById(searchResultId).apply {
            hidden = true
        }
        searchResultService.updateSearchResult(searchResult)
        return redirectToCrawlerPage()
    }

    private fun redirectToCrawlerPage(): String {
        return "redirect:/crawler"
    }

}
