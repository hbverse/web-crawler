package com.hbverse.hb.apps.wiener_zeitung.web

import com.hbverse.hb.apps.wiener_zeitung.entity.SearchKey
import com.hbverse.hb.apps.wiener_zeitung.entity.SearchResult
import com.hbverse.hb.apps.wiener_zeitung.service.SearchPeriod
import org.slf4j.Logger
import org.springframework.stereotype.Component

@Component
class DetailSearchCrawler(
    private val wienerZeitungWebClient: WienerZeitungWebClient,
    private val log: Logger
) {

    fun searchFor(searchKeys: List<SearchKey>, period: SearchPeriod): List<SearchResult> {
        return wienerZeitungWebClient.use { client ->
            searchKeys.flatMap { searchKey ->
                val searchPage = client.getDetailsSearchPage()
                searchPage.fillSearchForm(searchKey, period)
                val resultPages = searchPage.search()
                log.info("Found ${resultPages.totalResultCount} results on ${resultPages.totalResultPagesCount} pages")
                return@flatMap resultPages.flatMap { it.results }
            }
        }
    }

}
