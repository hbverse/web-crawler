package com.hbverse.hb.apps.wiener_zeitung.mail

import com.hbverse.hb.apps.configuration.AppProperties
import com.hbverse.hb.apps.configuration.MailProperties
import com.hbverse.hb.apps.wiener_zeitung.entity.SearchResult
import org.springframework.mail.javamail.JavaMailSender
import org.springframework.stereotype.Component
import org.thymeleaf.TemplateEngine
import org.thymeleaf.context.Context
import javax.mail.Message
import javax.mail.internet.InternetAddress

@Component
class CrawlerMailSender(
    private val javaMailSender: JavaMailSender,
    private val templateEngine: TemplateEngine,
    private val mailProperties: MailProperties,
    private val appProperties: AppProperties
) {

    fun sendSearchResults(searchResults: List<SearchResult>) {
        val message = javaMailSender.createMimeMessage().apply {
            setRecipients(Message.RecipientType.TO, mailProperties.recipients.map { InternetAddress(it) }.toTypedArray())
            setSubject(mailProperties.subject, CHARSET)
            setContent(processMailTemplate(searchResults), CONTENT_TYPE)
            setFrom(InternetAddress(mailProperties.sender, mailProperties.personal))
        }
        javaMailSender.send(message)
    }

    private fun processMailTemplate(searchResults: List<SearchResult>): String {
        val context = Context().apply {
            setVariable("searchResults", searchResults)
            setVariable("baseUrl", appProperties.baseUrl)
        }
        return templateEngine.process(TEMPLATE_NAME, context)
    }

    companion object {
        private const val TEMPLATE_NAME = "crawler/mail"
        private const val CHARSET = "UTF-8"
        private const val CONTENT_TYPE = "text/html; charset=$CHARSET"
    }

}

