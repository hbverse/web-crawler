package com.hbverse.hb.apps.wiener_zeitung.entity

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType.IDENTITY
import javax.persistence.Id

@Entity
class SearchKey(
        @Id
        @GeneratedValue(strategy = IDENTITY)
        val id: Long? = null,
        var value: String = "",
        val exclude: Boolean = false
) {
    fun getFormattedValue(): String {
        return value.capitalize()
    }
}
