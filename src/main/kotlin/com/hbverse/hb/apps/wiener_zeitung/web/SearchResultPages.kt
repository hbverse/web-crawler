package com.hbverse.hb.apps.wiener_zeitung.web

import com.gargoylesoftware.htmlunit.WebClient
import com.gargoylesoftware.htmlunit.html.DomNode
import com.gargoylesoftware.htmlunit.html.HtmlAnchor
import com.gargoylesoftware.htmlunit.html.HtmlPage
import com.hbverse.hb.apps.wiener_zeitung.entity.SearchKey

class SearchResultPages(
    private val resultsLandingPage: HtmlPage,
    private val webClient: WebClient,
    private val searchKey: SearchKey,
    private val baseUrl: String
) : Iterable<SearchResultPage> {

    val totalResultCount by lazy {
        val noResultsFoundIndicator: DomNode? = resultsLandingPage.getElementById(HtmlElements.NO_RESULTS_INDICATOR_ID)
        if (noResultsFoundIndicator != null) return@lazy 0L
        val resultContainer: DomNode? = resultsLandingPage.getElementById(HtmlElements.RESULT_COUNT_CONTAINER_ID)
        if (resultContainer != null) return@lazy resultContainer.childNodes.first().asText().removeSuffix("Treffer").trim().toLong()
        throw IllegalStateException("Could not extract result count. Required elements could not be found on the page.")
    }

    val totalResultPagesCount by lazy {
        if (totalResultCount % RESUlTS_PER_PAGE == 0L) {
            totalResultCount / RESUlTS_PER_PAGE
        } else {
            totalResultCount / RESUlTS_PER_PAGE + 1L
        }
    }

    private val resultPageUrl by lazy {
        if (totalResultPagesCount < 2) return@lazy null
        val nextPageLink = resultsLandingPage.getElementById(HtmlElements.NEXT_RESULT_PAGE_ID) as HtmlAnchor
        val url = nextPageLink.hrefAttribute.replace(Regex("PID=.+"), PAGE_INDEX_PLACEHOLDER)
        return@lazy "$baseUrl/$url"
    }

    override fun iterator(): Iterator<SearchResultPage> {
        return ResultPagesIterator(totalResultPagesCount, this::getResultPage)
    }

    operator fun get(index: Int) = getResultPage(index.toLong())

    private fun getResultPage(pageIndex: Long): SearchResultPage {
        if (pageIndex < 0 || pageIndex >= totalResultPagesCount) throw IndexOutOfBoundsException("Result page index is out of bounds.")
        return if (pageIndex == 0L) {
            SearchResultPage(resultsLandingPage, searchKey, baseUrl, webClient)
        } else {
            requireNotNull(resultPageUrl) { "Expected resultPageUrl to be present if more than one result page was yielded." }
            val url = resultPageUrl!!.replace(PAGE_INDEX_PLACEHOLDER, "PID=${pageIndex + 1}")
            val page = webClient.getPage<HtmlPage>(url)
            return SearchResultPage(page, searchKey, baseUrl, webClient)
        }
    }

    private object HtmlElements {
        const val RESULT_COUNT_CONTAINER_ID = "cnt"
        const val NO_RESULTS_INDICATOR_ID = "ctl00_MainRegionContent_litNothingFound"
        const val NEXT_RESULT_PAGE_ID = "ctl00_MainRegionContent_ucSearchResult_repSearchResults_ctl51_rptFootPager_ctl06_hlNext"
    }

    companion object {
        private const val RESUlTS_PER_PAGE = 50L
        private const val PAGE_INDEX_PLACEHOLDER = "PID=PAGE_INDEX"
    }

    class ResultPagesIterator(
        private val totalResultPagesCount: Long,
        private val getResultPage: (pageIndex: Long) -> SearchResultPage
    ) : Iterator<SearchResultPage> {

        private var currentIndex = 0L

        override fun hasNext(): Boolean {
            return currentIndex < totalResultPagesCount
        }

        override fun next(): SearchResultPage {
            return getResultPage(currentIndex++)
        }

    }

}


