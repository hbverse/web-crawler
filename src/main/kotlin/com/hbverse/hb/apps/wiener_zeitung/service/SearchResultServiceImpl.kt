package com.hbverse.hb.apps.wiener_zeitung.service

import com.hbverse.hb.apps.wiener_zeitung.entity.SearchKey
import com.hbverse.hb.apps.wiener_zeitung.entity.SearchResult
import com.hbverse.hb.apps.wiener_zeitung.repository.SearchResultRepository
import com.hbverse.hb.apps.wiener_zeitung.web.DetailSearchCrawler
import org.springframework.stereotype.Service
import javax.persistence.EntityNotFoundException

@Service
class SearchResultServiceImpl(
    private val repository: SearchResultRepository,
    private val detailSearchCrawler: DetailSearchCrawler
) : SearchResultService {

    override fun updateSearchResult(searchResult: SearchResult) {
        repository.save(searchResult)
    }

    override fun hideMatchingResults(excludeKey: SearchKey) {
        require(excludeKey.exclude) { "Can only hide results for search keys where exclude property is set to true." }
        val matchingResults = getAllSearchResults().filter { it.matches(excludeKey) }
        matchingResults.forEach { it.hide() }
        repository.saveAll(matchingResults)
    }

    override fun getSearchResultById(id: Long): SearchResult {
        return try {
            repository.getOne(id)
        } catch (exception: EntityNotFoundException) {
            throw IllegalArgumentException("Search Result with id=$id not found.")
        }
    }

    override fun getNewResults(searchKeys: List<SearchKey>, excludeKeys: List<SearchKey>, period: SearchPeriod): List<SearchResult> {
        val allSearchResults = detailSearchCrawler.searchFor(searchKeys, period)
        val relevantResults = allSearchResults
            .filterNot { it.matchesAny(excludeKeys) }
            .filterNot { searchResultDoesExist(it) }
        return repository.saveAll(relevantResults)
    }

    override fun getAllSearchResults(): List<SearchResult> {
        return repository.findAllByHiddenEqualsOrderByPublishedAtDesc(hidden = false)
    }

    private fun searchResultDoesExist(searchResult: SearchResult): Boolean {
        val matchingResult = repository.findOneByTitleOrUrl(searchResult.title, searchResult.url)
        return matchingResult.isPresent
    }

}
