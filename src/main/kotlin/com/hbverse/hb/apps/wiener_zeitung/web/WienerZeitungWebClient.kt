package com.hbverse.hb.apps.wiener_zeitung.web

import com.gargoylesoftware.htmlunit.WebClient
import org.springframework.stereotype.Component
import java.io.Closeable

@Component
class WienerZeitungWebClient(
    private val webClient: WebClient
) : Closeable {

    override fun close() {
        webClient.topLevelWindows.forEach { it.close() }
    }

    fun getDetailsSearchPage(): DetailsSearchPage {
        return DetailsSearchPage(webClient, baseUrl = BASE_URL)
    }

    companion object {
        private const val BASE_URL = "https://www.firmenmonitor.at"
    }

}
