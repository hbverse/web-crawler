package com.hbverse.hb.apps.wiener_zeitung.repository

import com.hbverse.hb.apps.wiener_zeitung.entity.SearchKey
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface SearchKeyRepository : CrudRepository<SearchKey, Long> {

    fun findAllByExcludeEqualsOrderByValueAsc(exclude: Boolean = false): List<SearchKey>

}
