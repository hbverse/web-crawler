package com.hbverse.hb.apps.wiener_zeitung.service

import com.hbverse.hb.apps.wiener_zeitung.entity.SearchKey
import com.hbverse.hb.apps.wiener_zeitung.repository.SearchKeyRepository
import org.springframework.stereotype.Service

@Service
class JpaSearchKeyService(
    private val repository: SearchKeyRepository
) : SearchKeyService {
    override fun getAllSearchKeys(): List<SearchKey> {
        return repository.findAllByExcludeEqualsOrderByValueAsc(exclude = false)
    }

    override fun getAllExcludingKeys(): List<SearchKey> {
        return repository.findAllByExcludeEqualsOrderByValueAsc(exclude = true)
    }

    override fun save(searchKey: SearchKey) {
        repository.save(searchKey)
    }

    override fun delete(id: Long) {
        repository.deleteById(id)
    }
}
