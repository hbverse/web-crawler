package com.hbverse.hb.apps.wiener_zeitung.web

import com.gargoylesoftware.htmlunit.WebClient
import com.gargoylesoftware.htmlunit.html.*
import com.hbverse.hb.apps.wiener_zeitung.entity.SearchKey
import com.hbverse.hb.apps.wiener_zeitung.service.SearchPeriod

class DetailsSearchPage(
    private val webClient: WebClient,
    baseUrl: String
) {

    private var searchKey: SearchKey? = null
    private val sanitizedBaseUrl = baseUrl.removeSuffix("/")
    private val page by lazy {
        webClient.getPage<HtmlPage>("$sanitizedBaseUrl/$PAGE_PATH")
    }

    fun fillSearchForm(searchKey: SearchKey, period: SearchPeriod) {
        this.searchKey = searchKey
        val companyInput = page.getElementById(HtmlElements.COMPANY_NAME_ID) as HtmlInput
        companyInput.valueAttribute = searchKey.value
        val timeSpanSelect = page.getElementById(HtmlElements.TIME_SPAN_ID) as HtmlSelect
        timeSpanSelect.selectedIndex = period.index
        val individualRadio = page.getElementById(HtmlElements.INDIVIDUAL_RADIO_ID) as HtmlRadioButtonInput
        individualRadio.click<HtmlPage>()
        val newCompanyCheckbox = page.getElementById(HtmlElements.NEW_COMPANY_CHECKBOX_ID) as HtmlCheckBoxInput
        newCompanyCheckbox.click<HtmlPage>()
    }

    fun search(): SearchResultPages {
        val searchKey = this.searchKey
        requireNotNull(searchKey) { throw IllegalStateException("Search form must be filled before searching.") }
        val searchLink = page.getElementById(HtmlElements.SEARCH_LINK_ID)
        val resultPage = searchLink.click<HtmlPage>()
        return SearchResultPages(resultPage, webClient, searchKey, sanitizedBaseUrl)
    }


    companion object {
        private const val PAGE_PATH = "detailsearch.aspx"
    }

    private object HtmlElements {
        const val COMPANY_NAME_ID = "ctl00_MainRegionContent_ucSearch_AutoCompleteCompanyName"
        const val TIME_SPAN_ID = "ctl00_MainRegionContent_ucSearch_ddlPeriods"
        const val INDIVIDUAL_RADIO_ID = "ctl00_MainRegionContent_ucSearch_rblSearchType_1"
        const val NEW_COMPANY_CHECKBOX_ID = "ctl00_MainRegionContent_ucSearch_lvAjaxCategories_ctrl3_ctl01_cbxAjaxCategory"
        const val SEARCH_LINK_ID = "ctl00_MainRegionContent_ucSearch_btnTextSearch"
    }

}
