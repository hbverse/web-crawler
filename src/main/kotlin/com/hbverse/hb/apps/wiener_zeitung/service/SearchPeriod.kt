package com.hbverse.hb.apps.wiener_zeitung.service

enum class SearchPeriod(val index: Int, val label: String) {
    CURRENT_WEEK(0, "Diese Woche"),
    LAST_WEEK(1, "Letzte Woche"),
    LAST_TEN_DAYS(2, "Letzten 10 Tage"),
    LAST_MONTH(3, "Letzter Monat"),
    ALL_RECORDS(4, "Alle Daten"),
    LAST_YEAR(5, "Letztes Jahr"),
    SINCE_2010(6, "Seit 2010"),
    SINCE_2009(6, "Seit 2009"),
    SINCE_2008(6, "Seit 2008"),
    SINCE_2007(6, "Seit 2007"),
    SINCE_2006(6, "Seit 2006"),
    SINCE_2005(6, "Seit 2005"),
    SINCE_2004(6, "Seit 2004"),
    SINCE_2003(6, "Seit 2003"),
    SINCE_2002(6, "Seit 2002"),
    SINCE_2001(6, "Seit 2001"),
    SINCE_2000(6, "Seit 2000");

    companion object {
        fun ofIndex(index: Int): SearchPeriod {
            val period = values().find { it.index == index }
            requireNotNull(period) { "SearchPeriod with index=$index does not exist." }
            return period
        }
    }

}
