package com.hbverse.hb.apps.wiener_zeitung.service

import com.hbverse.hb.apps.wiener_zeitung.entity.SearchKey

interface SearchKeyService {

    fun getAllSearchKeys(): List<SearchKey>

    fun getAllExcludingKeys(): List<SearchKey>

    fun save(searchKey: SearchKey)

    fun delete(id: Long)

}
