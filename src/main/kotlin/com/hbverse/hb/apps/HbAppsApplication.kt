package com.hbverse.hb.apps

import com.hbverse.hb.apps.configuration.AppProperties
import com.hbverse.hb.apps.configuration.MailProperties
import com.hbverse.hb.apps.configuration.SentryProperties
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer
import org.springframework.scheduling.annotation.EnableScheduling

@SpringBootApplication
@EnableScheduling
@EnableConfigurationProperties(AppProperties::class, MailProperties::class, SentryProperties::class)
class HbAppsApplication : SpringBootServletInitializer()

fun main(args: Array<String>) {
    runApplication<HbAppsApplication>(*args)

}




